import hashlib

from User import User

#lista_users = []


def menu():
    print("""======================MENU========================
1. Zarejestruj się
2. Zaloguj się
3. Lista użytkowników""")

    choose = int(input())

    if choose == 1:
        register()
    if choose == 2:
        login()
    if choose == 3:
        view_list_users()


def register():
    global lista_users
    email = str(input("podaj adres e-mail"))
    if is_email_in_list(email):
        print(
            "podany email już ma założone konto, spróbuj zarejestrować konto na inny e-mail lub zaloguj się poprawnie")
        menu()

    if "@" not in email:
        print("podałeś niepoprawny email spróbój jeszcze raz")
        register()
    name = str(input("podaj nick"))
    pass1 = str(input("podaj hasło"))
    pass2 = str(input("powtórz hasło"))
    if pass1 == pass2:
        print("Twoje konto zostało utworzone poprawnie")
        pass_hash = hashlib.sha3_256(pass2.encode('utf-8')).hexdigest()
        add_user(name, email, pass_hash)
        menu()

    else:
        print("podane hasła róznią się\nSpróbuj jeszcze raz")
        register()


def login():
    global lista_users
    print("aby się zalogować:")
    email = input("podaj adres e-mail")
    pass1 = input("podaj hasło")
    pass_hash = hashlib.sha3_256(pass1.encode('utf-8')).hexdigest()
    is_found = False
    plik = open("files/users.txt", "r")
    for line in plik:
        list = line.strip().split(";")
    #    print(list[2], pass_hash, list[1], email)
        if list[2] == pass_hash and list[1] == email:
            print("zalogowałeś się poprawnie\nWitaj ")
            user = User(list[0], list[1], list[2])
            print(user.przedstaw_sie())
            is_found = True
            break
    if not is_found:
        print("Nie znaleziono uzytkownika z podanymi danymi logowania")
    menu()

    plik.close()
    menu()



    # for element in lista_users:
    #     if element.user_pass == pass_hash and element.user_email == email:
    #         print("zalogowałeś się poprawnie\nWitaj ")
    #         print(element.przedstaw_sie())
    #         is_found = True
    #         break
    # if not is_found:
    #     print("Nie znaleziono uzytkownika z podanymi danymi logowania")
    # menu()


def view_list_users():
#    for element in lista_users:
 #       print(element.get_name_email())
    plik = open("files/users.txt", "r")
    for line in plik:
        list = line.strip().split(";")
        print(list[0],list[1])

    plik.close()
    menu()


def is_email_in_list(email):

    plik = open("files/users.txt", "r")
    for line in plik:
        list = line.split(";")
        #print(list[1])
        if list[1] == email:
            return True
            plik.close()
            break
    plik.close()
    return False

   # global lista_users
   #for element in lista_users:
   #     if element.user_email == email:
   #         return True
   #        break
   #return False

def add_user(name, email, pass_hash):
    global lista_users
    user = User(name, email, pass_hash)
    #lista_users.append(user)
    plik = open("files/users.txt", "a")
    plik.write(name+";"+email+";"+pass_hash+"\n")
    plik.close()


menu()
