#### Kryptograficzna funkcja skrótu.

Hashowanie - przekształcenie danego ciągu znakowego charakteryzujące się nieodwracalnością operacji.
W bazie danych nie przechowujemy haseł użytkowników, lecz skróty(hashe) tych haseł
Popularne funkcje hashujące:
1. MD5 - złamane
2. SHA2_256
3. SHA3_256
4. BCRYPT

Obecnie w firmach korzysta się z Bcrypt



Przykład funkcji md5

```
hashlib.md5("whatever".encode('utf-8')).hexdigest()
```



#### ZADANIE 1
Utwórz program z menu

======================MENU========================
1. Zarejestruj się
2. Zaloguj się


Wybierając 1. użytkownik zostanie poproszony 
adres email, name i dwa razy hasło
Jeśli 2 hasła się nie zgadzają to otrzyma komunikat
"podane hasło nie zgadza się"

Jeśli wpisze poprawnie hasła, to użytkownik zostanie utworzony (zapisany do listy)

Obiekt użytkownika powinien być w formie
User(id, name, email, password)



Wybierając 2. Użytkownik zostanie poproszony o email i hasło,
jeśli wpisze poprawny (użytkownik z takimi wartościami jest na liście)
To dostanie komunikat "witaj {{name}}"

#### ZADANIE 2
W menu z zadania 1 dodaj opcje Lista użytkowników, która wyświetli wszystkich użytkowników, ale bez wyświatlania hashy haseł

#### ZADANIE 3
Stwórz metodę `def is_email_in_list(email)`
która będzie przeszukiwała liste userów i jeśli email jest zajęty to zwróci true, w przeciwnym razie zwróci false.
Wykorzystaj metodę przy rejestracji




#### Praca na plikach
w pierwszym parametrze umieszczamy nazwe pliku, w drugim podajemy operacje jaka chcemy wykonac
1. `r` - oznacza tryb odczytu
1. `w` - oznacza tryb zapisu - tworzy plik jesli go nie ma
1. `a` - oznaczy tryb doczepiania do pliku (append)

 ```
plik = open("plik.txt", "r")
```


zawartości pliku linia po lini
```
for line in plik:
    print(line.strip())
```
Plik należy zawsze zamknąć, żeby dane nie
```
plik.close()
```

Aby zapisać tekst używamy metody write
```
plik = open("plik.txt", "w")
plik.write( "xxxxxxxxxx")
plik.close()
```

#### ZADANIE 4
Wykorzystaj wiedze dotyczącą pracy na plikach w zadaniu 1 przechowując userów w pliku.
Możesz przetrzymywać ich wartości po `;` 
```
adam;adam@wp.pl;hashedpassword
```

Do przetworzenia danych podczas odczytu wykorzystaj metode split()


#### ZADANIE DODATKOWE 1
Stwórz program, który będzie wykonywał atak słownikowy md5


Na początku użytkownik wpisze hash w md5.
Program przeszuka słownik z wartościami i jeśli znajdzie hasło, to wyświetli komunikat "Złamano hasło: {{password}}

Słowniki definiuje się w sposób
```
slownik = {}
```
Aby dodawać rekordy używamy
```
slownik["Jan"] = 624263236236
slownik["Jacek"] = 1231231232
```

Aby przeszukać słownik używamy 
```
for key, value in slownik.iteritems():
    print (key, " ", value)
```



