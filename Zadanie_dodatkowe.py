import hashlib

from Haslib import word_to_md5

dictionary = {}



def menu():
    print("""
    =======================MENU======================
    1. Dodaj słowo do słownika MD5
    2. Wyświetl słownik MD5
    3. Zdekoduj hasha
    """)

    choose = int(input())

    if choose == 1:
        add_word_to_dictionary()
    if choose == 2:
        show_dictionary()
    if choose == 3:
        decode_word()

def add_word_to_dictionary():
    print("stwórz słownik z hashami")
    word = input("podaj slowo do zaszyfrowania i do dodania do slowanika")
    dictionary[word]= word_to_md5(word)
    menu()

def show_dictionary():
    for i in dictionary:
        print(i, dictionary[i])
    menu()
def decode_word():
    word = input("podaj hash którego chcesz rozszyfrować")
    for i in dictionary:
        if dictionary[i] == word:
            print(i)
    menu()
menu()