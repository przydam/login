#haslo = "test"
#print("hasło md5", hashlib.md5(haslo.encode('utf-8')).hexdigest())
#print("hasło sha256", hashlib.sha256(haslo.encode('utf-8')).hexdigest())
#print("hasło sha3_256", hashlib.sha3_256(haslo.encode('utf-8')).hexdigest())
import hashlib


def word_to_md5(word):
    return hashlib.md5(word.encode('utf-8')).hexdigest()